// Created by: Sunday Emmanuel Hezekiah
#include "main.h"
#include <stdlib.h>
#include <stdio.h>

Student * createStudentRecord(int id, Student *students[]) {
    Student *student = (Student *) malloc(sizeof(Student));
    printf("\nEnter Student Name: ");
    scanf(" %[^\n]", student->name);
    fflush(stdin);
    
    printf("\nEnter Student Date of Birth: ");
    scanf("%s", student->dateOfBirth);
    fflush(stdin);
    printf("\nEnter Student Registration Number: ");
    scanf("%s", student->registrationNumber);
    fflush(stdin);
    printf("\nEnter Student Program Code: ");
    scanf("%s", student->programCode);
    fflush(stdin);
    printf("\nEnter Student Annual Tuition: ");
    scanf("%f", &student->annualTuition);
    fflush(stdin);
    
    students[id] = student;
    student->id = id;
    if (student != NULL)
        printf("\n\nStudent Record Created Successfully!\n\n");
    else
        printf("\n\nStudent Record Creation Failed!\n\n");
    return student;
}
