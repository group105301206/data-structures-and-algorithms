#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>


#define MAX_NAME_LENGTH 51 // Including null terminator
#define DATE_LENGTH 11
#define REGISTRATION_LENGTH 11
#define PROGRAM_CODE_LENGTH 5

typedef struct Node {
    // Fields for student information
    char name[MAX_NAME_LENGTH]; // Name of the student
    char dateOfBirth[DATE_LENGTH]; // Date of birth of the student
    char registrationNumber[REGISTRATION_LENGTH]; // Registration number of the student
    char programCode[PROGRAM_CODE_LENGTH]; // Program code of the student
    float annualTuition; // Annual tuition fee of the student
    struct Node* next; // Pointer to the next node in the linked list
} Node;