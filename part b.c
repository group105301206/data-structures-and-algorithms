#include <stdio.h>
#include <string.h>

// Function prototypes
Node* createStudent();
void readStudents(Node *head);
void updateStudent(Node *head);
void deleteStudent(Node **head);
void searchStudentByRegNumber(Node *head);
void sortStudents(Node *head);
void exportStudentsToFile(Node *head);

int main() {
    Node *head = NULL;
    int choice;

    do {
        printf("\n--------STUDENT MANAGEMENT SYSTEM_GROUP 10--------\n");
        printf("\nMain Menu\n");
        printf("1. Create student\n");
        printf("2. Read students\n");
        printf("3. Update student\n");
        printf("4. Delete student\n");
        printf("5. Search student by registration number\n");
        printf("6. Sort students\n");
        printf("7. Export students to file\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                head = createStudent(head);
                break;
            case 2:
                readStudents(head);
                break;
            case 3:
                updateStudent(head);
                break;
            case 4:
                deleteStudent(&head);
                break;
            case 5:
                searchStudentByRegNumber(head);
                break;
            case 6:
                sortStudents(head);
                break;
            case 7:
                exportStudentsToFile(head);
                break;
            case 8:
                printf("Exiting program...\n");
                break;
            default:
                printf("Invalid choice. Please enter a number between 1 and 8.\n");
        }
    } while (choice != 8);

    // Free memory before exiting
    Node *current = head;
    while (current != NULL) {
        Node *temp = current;
        current = current->next;
        free(temp);
    }

    return 0;
}



    
