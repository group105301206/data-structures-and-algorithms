#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define REGISTRATION_LENGTH 20

// Define the structure of a Node
typedef struct Node {
    char registrationNumber[REGISTRATION_LENGTH];
    char name[50];
    char dateOfBirth[20];
    char programCode[10];
    float annualTuition;
    struct Node *next;
} Node;
// Function to sort students by name
void sortStudents(Node *head, char sortBy[]) {
    if (head == NULL || head->next == NULL) {
        printf("No students to sort or only one student in the list.\n");
        return;
    }

    Node *ptr1, *ptr2;
    int swapped;

    do {
        swapped = 0;
        ptr1 = head;

        while (ptr1->next != NULL) {
            ptr2 = ptr1->next;
            if (strcmp(sortBy, "name") == 0 && strcmp(ptr1->name, ptr2->name) > 0) {
                // Swap data
                Node temp = *ptr1;
                *ptr1 = *ptr2;
                *ptr2 = temp;
                swapped = 1;
            }
            ptr1 = ptr1->next;
        }
    } while (swapped);
}

// Function to print the selected field of the list
void printSelectedField(Node *head, char field[]) {
    Node *temp = head;
    printf("[");
    while (temp != NULL) {
        if (strcmp(field, "name") == 0)
            printf("%s", temp->name);
        else if (strcmp(field, "registration") == 0)
            printf("%s", temp->registrationNumber);

        if (temp->next != NULL)
            printf(", ");
        temp = temp->next;
    }
    printf("]\n");
}

// Function to create a new node
Node* createNode(char *regNum, char *name, char *dob, char *programCode, float tuition) {
    Node *newNode = (Node*)malloc(sizeof(Node));
    if (newNode == NULL) {
        printf("Memory allocation failed.\n");
        exit(1);
    }
    strcpy(newNode->registrationNumber, regNum);
    strcpy(newNode->name, name);
    strcpy(newNode->dateOfBirth, dob);
    strcpy(newNode->programCode, programCode);
    newNode->annualTuition = tuition;
    newNode->next = NULL;
    return newNode;
}

// Function to insert a new node at the end of the list
void insertAtEnd(Node **head, char *regNum, char *name, char *dob, char *programCode, float tuition) {
    Node *newNode = createNode(regNum, name, dob, programCode, tuition);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    Node *temp = *head;
    while (temp->next != NULL) {
        temp = temp->next;
    }
    temp->next = newNode;
}

// Function to print the list
void printList(Node *head) {
    Node *temp = head;
    while (temp != NULL) {
        printf("Registration Number: %s\n", temp->registrationNumber);
        printf("Name: %s\n", temp->name);
        printf("Date of Birth: %s\n", temp->dateOfBirth);
        printf("Program Code: %s\n", temp->programCode);
        printf("Annual Tuition: %.2f\n", temp->annualTuition);
        printf("\n");
        temp = temp->next;
    }
}

int main() {
    Node *head = NULL;

    // Insert some sample data
    insertAtEnd(&head, "123456", "John Doe", "1990-01-01", "CS", 1500.00);
    insertAtEnd(&head, "789012", "Jane Smith", "1992-05-15", "IT", 1800.00);
    insertAtEnd(&head, "345678", "Alice Johnson", "1988-09-30", "EE", 1700.00);

    printf("List of students before sorting:\n");
    printList(head);

    // Sort the list by name
    sortStudents(head, "name");

    printf("List of students after sorting by name:\n");
    printList(head);

    // Print selected field after sorting
    printf("Names of students after sorting: ");
    printSelectedField(head, "name");

    // Free allocated memory
    Node *temp;
    while (head != NULL) {
        temp = head;
        head = head->next;
        free(temp);
    }

    return 0;
}