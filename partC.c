#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>


void searchStudentByRegNumber(Node *head){
    if (head == NULL){
        printf("No student to search\n");
        return;
    }
char regnum[REGISTRATION_LENGTH];
printf("Enter registration number of the student to search: ");
scanf( "%s", regNum);
Node *current = head;
while (current !=NULL){ 
    if (strcmp(current->registrationNumber,regNum)==0){
        printf("Name: %s\n", current->name);
        printf("Date of Birth:%s\n", current ->dateOfBirth);
        printf("Program Code:%s\n", current ->programCode);
        printf("Annual Tuition:%.2f\n",current ->annualTuition);
        return;
    }
    current = current->next;
}
printf("Student with registration number %s not found.\n",regNum);
}







