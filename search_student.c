#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define REGISTRATION_LENGTH 20

// Define the structure of a Node
typedef struct Node {
    char registrationNumber[REGISTRATION_LENGTH];
    char name[50];
    char dateOfBirth[20];
    char programCode[10];
    float annualTuition;
    struct Node *next;
} Node;

// Function to search for a student by registration number
void searchStudentByRegNumber(Node *head) {
    if (head == NULL) {
        printf("No students to search.\n");
        return;
    }

    char regNum[REGISTRATION_LENGTH];
    printf("Enter registration number of the student to search: ");
    scanf("%s", regNum);

    Node *current = head;
    while (current != NULL) {
        if (strcmp(current->registrationNumber, regNum) == 0) {
            printf("Name: %s\n", current->name);
            printf("Date of Birth: %s\n", current->dateOfBirth);
            printf("Program Code: %s\n", current->programCode);
            printf("Annual Tuition: %.2f\n", current->annualTuition);
            return;
        }
        current = current->next;
    }

    printf("Student with registration number %s not found.\n", regNum);
}

// Function to create a new node
Node* createNode(char *regNum, char *name, char *dob, char *programCode, float tuition) {
    Node *newNode = (Node*)malloc(sizeof(Node));
    if (newNode == NULL) {
        printf("Memory allocation failed.\n");
        exit(1);
    }
    strcpy(newNode->registrationNumber, regNum);
    strcpy(newNode->name, name);
    strcpy(newNode->dateOfBirth, dob);
    strcpy(newNode->programCode, programCode);
    newNode->annualTuition = tuition;
    newNode->next = NULL;
    return newNode;
}


// Function to insert a new node at the end of the list
void insertAtEnd(Node **head, char *regNum, char *name, char *dob, char *programCode, float tuition) {
    Node *newNode = createNode(regNum, name, dob, programCode, tuition);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    Node *temp = *head;
    while (temp->next != NULL) {
        temp = temp->next;
    }
    temp->next = newNode;
}


// Function to print the list
void printList(Node *head) {
    Node *temp = head;
    while (temp != NULL) {
        printf("Registration Number: %s\n", temp->registrationNumber);
        printf("Name: %s\n", temp->name);
        printf("Date of Birth: %s\n", temp->dateOfBirth);
        printf("Program Code: %s\n", temp->programCode);
        printf("Annual Tuition: %.2f\n", temp->annualTuition);
        printf("\n");
        temp = temp->next;
    }
} 


int main() {
    Node *head = NULL;

    // Insert some sample data
    insertAtEnd(&head, "123456", "John Doe", "1990-01-01", "CS", 1500.00);
    insertAtEnd(&head, "789012", "Jane Smith", "1992-05-15", "IT", 1800.00);
    insertAtEnd(&head, "345678", "Alice Johnson", "1988-09-30", "EE", 1700.00);

    printf("List of students:\n");
    printList(head);

    // Search for a student
    searchStudentByRegNumber(head);

    // Free allocated memory
    Node *temp;
    while (head != NULL) {
        temp = head;
        head = head->next;
        free(temp);
    }

    return 0;
} 