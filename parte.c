#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>


void exportStudentsToFile(Node *head) {
    if (head == NULL) {
        printf("No students to export.\n");
        return;
    }

    FILE *file = fopen("students.csv", "w");
    if (file == NULL) {
        printf("Error opening file.\n");
        return;
    }

    Node *current = head;
    while (current != NULL) {
        fprintf(file, "%s,%s,%s,%s,%.2f\n", current->name, current->dateOfBirth,
                current->registrationNumber, current->programCode, current->annualTuition);
        current = current->next;
    }

    fclose(file);
    printf("Students exported to file successfully.\n");
}
