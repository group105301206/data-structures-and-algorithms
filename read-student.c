// Created by: Ashley Naluwuja
#include "main.h"
#include <stdio.h>
#include <string.h>

void readStudentRecord(Student *students[]) {
    char name[51];
    printf("\n\nEnter the Student Name: ");
    scanf(" %[^\n]", name);
    fflush(stdin);

    for (int i = 0; i < 100; i++) {
        if (students[i] != NULL && strcmp(students[i]->name, name) == 0) {
            printf("\n\nStudent Name: %s", students[i]->name);
            printf("\nStudent Date of Birth: %s", students[i]->dateOfBirth);
            printf("\nStudent Registration Number: %s", students[i]->registrationNumber);
            printf("\nStudent Program Code: %s", students[i]->programCode);
            printf("\nStudent Annual Tuition: %.2f\n\n", students[i]->annualTuition);
            printf("\n\nStudent Record Found Successfully!\n\n");
            return;
        }
    }
    printf("\n\nStudent Record Not Found!\n\n");
}