#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Define the structure of a Node
typedef struct Node {
    char name[50];
    // Add other data members as needed
    struct Node *next;
} Node;

// Function to sort students by name
Node* sortStudents(Node *head) {
    if (head == NULL || head->next == NULL) {
        printf("No students to sort or only one student in the list.\n");
        return head;
    }

    Node *sortedList = NULL;
    Node *current = head;

    while (current != NULL) {
        Node *next = current->next;

        if (sortedList == NULL || strcmp(current->name, sortedList->name) < 0) {
            current->next = sortedList;
            sortedList = current;
        } else {
            Node *temp = sortedList;
            while (temp->next != NULL && strcmp(current->name, temp->next->name) > 0) {
                temp = temp->next;
            }
            current->next = temp->next;
            temp->next = current;
        }

        current = next;
    }

    return sortedList;
}

// Function to create a new node
Node* createNode(char *name) {
    Node *newNode = (Node*)malloc(sizeof(Node));
    if (newNode == NULL) {
        printf("Memory allocation failed.\n");
        exit(1);
    }
    strcpy(newNode->name, name);
    newNode->next = NULL;
    return newNode;
}

// Function to insert a new node at the end of the list
void insertAtEnd(Node **head, char *name) {
    Node *newNode = createNode(name);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    Node *temp = *head;
    while (temp->next != NULL) {
        temp = temp->next;
    }
    temp->next = newNode;
}

// Function to print the list
void printList(Node *head) {
    Node *temp = head;
    while (temp != NULL) {
        printf("%s -> ", temp->name);
        temp = temp->next;
    }
    printf("NULL\n");
}

// Function to convert the linked list to an array
char** listToArray(Node *head, int *size) {
    *size = 0;
    Node *temp = head;
    while (temp != NULL) {
        (*size)++;
        temp = temp->next;
    }

    char **array = (char**)malloc(*size * sizeof(char*));
    if (array == NULL) {
        printf("Memory allocation failed.\n");
        exit(1);
    }

    temp = head;
    int i = 0;
    while (temp != NULL) {
        array[i] = strdup(temp->name); // Create a copy of the string
        temp = temp->next;
        i++;
    }

    return array;
}

// Function to free the memory allocated for the array
void freeArray(char **array, int size) {
    for (int i = 0; i < size; i++) {
        free(array[i]);
    }
    free(array);
}

int main() {
    Node *head = NULL;

    // Insert some sample data
    insertAtEnd(&head, "John");
    insertAtEnd(&head, "Alice");
    insertAtEnd(&head, "Bob");
    insertAtEnd(&head, "Zoe");
    insertAtEnd(&head, "Charlie");

    printf("Before sorting:\n");
    printList(head);

    // Sort the list and convert it to an array
    int size;
    char **sortedArray = listToArray(sortStudents(head), &size);

    printf("After sorting:\n");
    for (int i = 0; i < size; i++) {
        printf("%s -> ", sortedArray[i]);
    }
    printf("NULL\n");

    // Free allocated memory
    freeArray(sortedArray, size);

    return 0;
}
